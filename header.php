<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Homepage | Angle HTML</title>
    <link rel="icon" type="image/x-icon" href="assets/images/favicons/favicon.ico" />
    <link rel="icon" type="image/png" href="assets/images/favicons/favicon.png" />
    <!-- For iPhone 4 Retina display: -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/images/favicons/apple-touch-icon-114x114-precomposed.png">
    <!-- For iPad: -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/images/favicons/apple-touch-icon-72x72-precomposed.png">
    <!-- For iPhone: -->
    <link rel="apple-touch-icon-precomposed" href="assets/images/favicons/apple-touch-icon-60x60-precomposed.png">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,400italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/theme.css">
    <link rel="stylesheet" href="assets/css/color-defaults.min.css">
    <link rel="stylesheet" href="assets/css/swatch-blue-white.css">
    <link rel="stylesheet" href="assets/css/swatch-white-blue.css">
    <link rel="stylesheet" href="assets/css/fonts.css" media="screen">
  </head>
  <body>
    <header id="masthead" class="navbar navbar-sticky swatch-blue-white" role="banner" style="border-bottom: 5px solid #0076ca;">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".main-navbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="#" class="navbar-brand">
            <img src="assets/images/logo.png" alt="One of the best themes ever">
          </a>
        </div>
        <nav class="collapse navbar-collapse main-navbar" role="navigation">
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Accueil</a>
              <ul class="dropdown-menu">
                <li><a href="index.php#rejoindre">Nous rejoindre</a></li>
                <li><a href="index.php#stats">Statistiques</a></li>
                <li><a href="index.php#reseaux">Réseaux</a></li>
                <li><a href="index.php#faq">F.A.Q</a></li>
                <li><a href="index.php#support">Supports</a></li>
              </ul>
            </li>
            <li><a href="faq.php">F.A.Q</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Redirections</a>
              <ul class="dropdown-menu">
                <li><a href="#">fezfez</a></li>
                <li><a href="#">fzefzefz</a></li>
              </ul>
            </li>
          </ul>
        </nav>
      </div>
    </header>