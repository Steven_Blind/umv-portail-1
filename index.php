<?php include 'header.php' ?>
    <section class="section swatch-blue-white">
      <div class="background-media" style="background-image: url('assets/images/fond1.png'); background-repeat: ; background-size: ; background-attachment: ; background-position: ; background-size: cover;" data-start="background-position: 50% 0px"
      data-top-bottom="background-position: 50% -200px">
      </div>
      <div class="background-overlay" style="background-color:rgba(0, 132, 225,0.8)"></div>
      <div class="container">
          <header class="section-header underline">
              <h1 class="headline hyper hairline acc-title" style="margin-bottom: 2rem;">Une Meilleure Vie</h1>
              <h2 class="hairline acc-title"><i>Roleplay</i></h2>
          </header>
      </div>
    </section>
    <section id="rejoindre" class="section swatch-white-blue">
      <div class="decor-top">
        <svg class="decor" height="100%" preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100" width="100%" xmlns="http://www.w3.org/2000/svg">
          <path d="M0 0 L100 100 L0 100" stroke-width="0"></path>
        </svg>
      </div>
      <div class="container">
        <header class="section-header ">
          <h1 class="headline super hairline">Rejoindre la communauté</h1>
          <p class="">Suivez ces courtes étapes afin de venir partager l'expérience du roleplay sur MTASA.</p>
        </header>
        <div class="row">
          <ul class="list-unstyled row box-list horizontal-icon-border">
            <li class="col-md-3 text-center" data-os-animation="" data-os-animation-delay="">
              <div class="box-round box-medium">
                <div class="box-dummy"></div>
                <a class="box-inner " href="single-service.html">
                <i class="fa fa-arrow-circle-right" data-animation="bounce"></i>
                </a>
              </div>
              <h3 class="text-center ">
                <a class="simple-title" href="single-service.html">Installation de<br>GTA San Andreas</a>
              </h3>
            </li>
            <li class="col-md-3 text-center" data-os-animation="" data-os-animation-delay="">
              <div class="box-round box-medium">
                <div class="box-dummy"></div>
                <a class="box-inner " href="single-service.html">
                <i class="fa fa-arrow-circle-right" data-animation="bounce"></i>
                </a>
              </div>
              <h3 class="text-center ">
                <a class="simple-title" href="single-service.html">Installation de MTA:SA</a>
              </h3>
            </li>
            <li class="col-md-3 text-center" data-os-animation="" data-os-animation-delay="">
              <div class="box-round box-medium">
                <div class="box-dummy"></div>
                <a class="box-inner " href="single-service.html">
                <i class="fa fa-arrow-circle-right" data-animation="bounce"></i>
                </a>
              </div>
              <h3 class="text-center ">
                <a class="simple-title" href="single-service.html">Inscription à l'UCP</a>
              </h3>
            </li>
            <li class="col-md-3 text-center" data-os-animation="" data-os-animation-delay="">
              <div class="box-round box-medium">
                <div class="box-dummy"></div>
                <a class="box-inner " href="single-service.html">
                <i class="fa fa-check-circle" data-animation="bounce"></i>
                </a>
              </div>
              <h3 class="text-center ">
                <a class="simple-title" href="single-service.html">Connexion au serveur Teamspeak</a>
              </h3>
            </li>
          </ul>
          <center>
            <a class="btn btn-lg btn-primary btn-icon-right" href="#">
              Étapes détaillées pour nous rejoindre
              <span class="hex-alt hex-alt-big">
                <i class="fa fa-hand-o-right" data-animation="wobble"></i>
              </span>
            </a>
          </center>
        </div>
      </div>
    </section>
    <section id="stats" class="section swatch-blue-white has-top">
      <div class="decor-top">
        <svg class="decor" height="100%" preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100" width="100%" xmlns="http://www.w3.org/2000/svg">
          <path d="M0 100 L100 0 L100 100" stroke-width="0"></path>
        </svg>
      </div>
      <div class="container">
        <header class="section-header underline">
          <h1 class="headline super hairline">Statistiques</h1>
        </header>
        <div class="row-fluid">
          <div class="col-md-6">
            <div class="span12">
            </div>
          </div>
          <div class="col-md-6">
            <div class="span12">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>Informations sur le serveur</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>serveur.umv-rp.fr</td>
                    <td>IP</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Inscrits ces 7 derniers jours</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="reseaux" class="section swatch-white-blue">
      <div class="decor-top">
        <svg class="decor" height="100%" preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100" width="100%" xmlns="http://www.w3.org/2000/svg"><path d="M0 100 C50 0 50 0 100 100 Z" stroke-width="0"></path></svg>
      </div>
      <div class="container">
        <header class="section-header underline">
          <h1 class="headline super hairline">Reseaux</h1>
        </header>
        <div class="container">
          <div class="col-md-4">
            <h2>Facebook</h2>
            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FUneMeilleureVieRoleplay&tabs=timeline&width=340&height=300&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1622594474696157" width="340" height="300" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
          </div>
          <div class="col-md-4">
            <h2>Twitter</h2>
            <a class="twitter-timeline"  href="https://twitter.com/UMVRP" data-widget-id="706255825157955584">Tweets de @UMVRP</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
          </div>
          <div class="col-md-4">
            <h2>Teamspeak</h2>
            <div id="ts3viewer_1049669" style="height:300px;"></div>
            <script src="https://static.tsviewer.com/short_expire/js/ts3viewer_loader.js"></script>
            <script>
            var ts3v_url_1 = "https://www.tsviewer.com/ts3viewer.php?ID=1049669&text=757575&text_size=12&text_family=1&text_s_color=025db1&text_s_weight=normal&text_s_style=normal&text_s_variant=normal&text_s_decoration=none&text_i_color=&text_i_weight=normal&text_i_style=normal&text_i_variant=normal&text_i_decoration=none&text_c_color=&text_c_weight=normal&text_c_style=normal&text_c_variant=normal&text_c_decoration=none&text_u_color=000000&text_u_weight=normal&text_u_style=normal&text_u_variant=normal&text_u_decoration=none&text_s_color_h=&text_s_weight_h=bold&text_s_style_h=normal&text_s_variant_h=normal&text_s_decoration_h=none&text_i_color_h=000000&text_i_weight_h=bold&text_i_style_h=normal&text_i_variant_h=normal&text_i_decoration_h=none&text_c_color_h=&text_c_weight_h=normal&text_c_style_h=normal&text_c_variant_h=normal&text_c_decoration_h=none&text_u_color_h=&text_u_weight_h=bold&text_u_style_h=normal&text_u_variant_h=normal&text_u_decoration_h=none&iconset=default_colored_2014";
            ts3v_display.init(ts3v_url_1, 1049669, 100);
            </script>
          </div>
        </div>
      </div>
    </section>
    <section id="faq" class="section swatch-blue-white">
      <div class="decor-top">
        <svg class="decor" height="100%" preserveaspectratio="none" version="1.1" viewbox="0 0 100 100" width="100%" xmlns="http://www.w3.org/2000/svg">
          <path d="M0 0 C50 100 50 100 100 0  L100 100 0 100" stroke-width="0"></path>
        </svg>
      </div>
      <div class="container">
        <header class="section-header underline">
          <h1 class="headline super hairline">F.A.Q</h1>
        </header>
        <center>
          <a class="btn btn-lg btn-primary" href="#">
            Recherchez la réponse à vos questions sur notre F.A.Q
          </a>
        </center>
      </div>
    </section>
    <section id="support" class="section swatch-white-blue has-top">
      <div class="decor-top">
        <svg class="decor" height="100%" preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100" width="100%" xmlns="http://www.w3.org/2000/svg"><path d="M0 0 L50 100 L100 0 L100 100 L0 100" stroke-width="0"></path></svg>
      </div>
      <div class="container">
        <header class="section-header ">
          <h1 class="headline super hairline">Support</h1>
          <p class="">Voici les différents moyens de contacter notre équipe sur l'ensemble de nos plateformes.</p>
        </header>
        <div class="row">
          <ul class="list-unstyled row box-list ">
            <li class="col-md-3 text-center os-animation" data-os-animation="fadeInUp" data-os-animation-delay=".0s">
              <div class="box-round">
                <div class="box-dummy"></div>
                <a class="box-inner " href="single-service.html">
                  <img class="svg-inject" src="assets/images/design/custom-icons/tawk.png" alt="glasses" data-animation="bounce" />
                </a>
              </div>
              <h3 class="text-center ">
                <a class="acc-title" href="https://tawk.to/chat/56d99c7267d1eaa929502c66/default/?$_tawk_popout=true">Support&nbsp;&nbsp;Instantané</a>
              </h3>
              <p class="text-center">A utiliser pour toutes vos questions qui ont besoin d'une réponse rapide ou en direct de la part d'un administrateur.</p>
            </li>
            <li class="col-md-3 text-center os-animation" data-os-animation="fadeInUp" data-os-animation-delay=".3s">
              <div class="box-round">
              <div class="box-dummy"></div>
              <a class="box-inner " href="single-service.html">
                  <img class="svg-inject" src="assets/images/design/custom-icons/custom-icon-envelope.png" alt="a clock" data-animation="bounce" />
              </a>
              </div>
              <h3 class="text-center ">
                <a class="acc-title" href="http://forum.umv-rp.fr/index.php/topic,1339.0.html">Mail</a>
              </h3>
              <p class="text-center">Chaque staff possède une adresse mail personnelle pour toutes questions éventuelles. Celles-ci sont composées de leur pseudo principal. Exemple : exemple@umv-rp.fr</p>
            </li>
            <li class="col-md-3 text-center os-animation" data-os-animation="fadeInUp" data-os-animation-delay=".6s">
              <div class="box-round">
              <div class="box-dummy"></div>
              <a class="box-inner " href="single-service.html">
                  <img class="svg-inject" src="assets/images/design/custom-icons/supp.png" alt="a clock" data-animation="bounce" />
              </a>
              </div>
              <h3 class="text-center ">
                <a class="acc-title" href="https://umv-rp.fr/support">Centre Support</a>
              </h3>
              <p class="text-center">Pour créer une demande de mapping, un unban, un remboursement ou encore déclarer un bug. Ce service est destiné aux supports demandant une durée plus longue de traitement.</p>
            </li>
            <li class="col-md-3 text-center os-animation" data-os-animation="fadeInUp" data-os-animation-delay=".6s">
              <div class="box-round">
              <div class="box-dummy"></div>
              <a class="box-inner " href="single-service.html">
                  <img class="svg-inject" src="assets/images/design/custom-icons/teamspeak.png" alt="a clock" data-animation="bounce" />
              </a>
              </div>
              <h3 class="text-center ">
                <a class="acc-title" href="ts3server/ts.umv-rp.fr?port=9987">Teamspeak</a>
              </h3>
              <p class="text-center">Rejoignez les autres membres de la communauté et entrez directement en contact avec un membre du staff en rejoignant les salons dédiés.</p>
            </li>
          </ul>
        </div>
      </div>
    </section>
<?php include 'footer.php' ?>
